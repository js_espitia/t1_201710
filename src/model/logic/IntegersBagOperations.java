package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {


	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}

	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}


	//mod1
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int numero;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				numero = iter.next();
				if(min > numero){
					min = numero;
				}
			}
		}
		return min;
	}


	//mod2
	public int howManyEven(IntegersBag bag){
		int ans = 0;
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				int actual = iter.next();
				if((actual % 2) == 0){ans++;}
			}
		}
		return ans;
	}


	//mod3	
	public int howManyOdds(IntegersBag bag){
		int ans = 0;
		if(bag!=null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				int actual = iter.next();
				if((actual % 2) != 0){ans++;}
			}
		}
		return ans;
	}

}
