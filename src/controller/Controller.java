package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static int getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	
	//mod1
	public static int getMin(IntegersBag bag){
		return model.getMin(bag);
	}
	
	//mod2
	public static int howManyEven(IntegersBag bag){
		return model.howManyEven(bag);
	}
	
	//mod3
	public static int howManyOdds(IntegersBag bag){
		return model.howManyOdds(bag);
	}
	
	
	
}
